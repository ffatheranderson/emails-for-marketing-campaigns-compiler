package emails_for_marketing_campaigns_compiler;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;

public class CsvFileChooser extends JFileChooser {
    public CsvFileChooser() {
        FileNameExtensionFilter csvFilter = new FileNameExtensionFilter("csv files", "csv");
        addChoosableFileFilter(csvFilter);
        setAcceptAllFileFilterUsed(false);
        setFileFilter(csvFilter);
    }
}
