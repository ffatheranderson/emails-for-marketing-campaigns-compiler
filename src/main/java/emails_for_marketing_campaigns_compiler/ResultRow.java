package emails_for_marketing_campaigns_compiler;

public class ResultRow {
    final String name;
    final String email;
    final String organization;

    public ResultRow(String name, String email, String organization) {
        this.name = name;
        this.email = email;
        this.organization = organization;
    }

    @Override
    public String toString() {
        return "ResultRow{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", organization='" + organization + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ResultRow resultRow = (ResultRow) o;

        if (name != null ? !name.equals(resultRow.name) : resultRow.name != null) return false;
        if (!email.equals(resultRow.email)) return false;
        return organization != null ? organization.equals(resultRow.organization) : resultRow.organization == null;
    }

    @Override
    public int hashCode() {
        return email.hashCode();
    }
}
