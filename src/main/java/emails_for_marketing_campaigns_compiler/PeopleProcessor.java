package emails_for_marketing_campaigns_compiler;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.apache.commons.lang3.StringUtils.*;

public class PeopleProcessor {

    public List<String[]> getPeople(String pathToPeopleFile, final Set<String> organizations) throws IOException {
        try (CSVReader reader = new CSVReader(new FileReader(pathToPeopleFile))) {

            Iterator<String[]> iterator = reader.iterator();
            int idxOfEmailColumn = -1;
            int idxOfNameColumn = -1;
            int idxOfOrganizationColumn = -1;

            if (!iterator.hasNext())
                throw new RuntimeException("Deals file is empty.");

            String[] titles = iterator.next();

            for (int i = 0; i < titles.length; i++) {
                if (idxOfEmailColumn != -1 && idxOfNameColumn != -1 && idxOfOrganizationColumn != -1)
                    break;

                String title = titles[i].trim();
                if (equalsIgnoreCase("email", title))
                    idxOfEmailColumn = i;
                else if (equalsIgnoreCase("name", title))
                    idxOfNameColumn = i;
                else if (equalsIgnoreCase("organization", title))
                    idxOfOrganizationColumn = i;
            }

            if (idxOfEmailColumn == -1)
                throw new RuntimeException("People file does not have \"Email\" column.");
            if (idxOfNameColumn == -1)
                throw new RuntimeException("People file does not have \"Name\" column.");
            if (idxOfOrganizationColumn == -1)
                throw new RuntimeException("People file does not have \"Organization\" column.");


            if (!iterator.hasNext())
                throw new RuntimeException("People file has only titles but no data rows.");

            final int idxOfEmailColumnFinal = idxOfEmailColumn;
            final int idxOfNameColumnFinal = idxOfNameColumn;
            final int idxOfOrganizationColumnFinal = idxOfOrganizationColumn;

            return StreamSupport.stream(Spliterators.spliteratorUnknownSize(iterator, 0), true)
                                .filter(row -> isNotBlank(row[idxOfEmailColumnFinal])
                                        && organizations
                                        .contains(row[idxOfOrganizationColumnFinal].trim().toLowerCase()))
                                .sorted(new ResultComparator(idxOfOrganizationColumnFinal))
                                .map(row -> new String[]{row[idxOfNameColumnFinal].trim()
                                        , row[idxOfEmailColumnFinal].trim().toLowerCase()
                                        , row[idxOfOrganizationColumnFinal].trim()})
                                .collect(Collectors.toList());
        }
    }

    public void writeResult(List<String[]> resultRows, String pathToResultFile) throws IOException {
        try (CSVWriter resultWriter = new CSVWriter(new FileWriter(pathToResultFile))) {
            resultRows.add(0, new String[]{"Name", "Email", "Organization"});
            resultWriter.writeAll(resultRows);
        }
    }

    static class ResultComparator implements Comparator<String[]> {

        final int ORGANIZATION_FIELD_IDX;

        public ResultComparator(int organizationFieldIdx) {
            this.ORGANIZATION_FIELD_IDX = organizationFieldIdx;
        }

        @Override
        public int compare(String[] o1, String[] o2) {
            return o1[ORGANIZATION_FIELD_IDX].trim().toLowerCase()
                                             .compareTo(o2[ORGANIZATION_FIELD_IDX].trim().toLowerCase());
        }
    }
}
