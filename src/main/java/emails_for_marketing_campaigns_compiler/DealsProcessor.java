package emails_for_marketing_campaigns_compiler;

import com.opencsv.CSVReader;

import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.Spliterators;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.apache.commons.lang3.StringUtils.equalsIgnoreCase;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

public class DealsProcessor {

    public Set<String> getOrganizationsSet(String dealsFilePath
            , Set<String> pipelinesFilter
            , Set<String> stagesFilter) throws IOException {

        try (CSVReader reader = new CSVReader(new FileReader(dealsFilePath))) {

            Iterator<String[]> iterator = reader.iterator();
            int idxOfEmailColumn = -1;
            int idxOfNameColumn = -1;
            int idxOfOrganizationColumn = -1;

            if (!iterator.hasNext())
                throw new RuntimeException("People file is empty.");

            String[] titles = iterator.next();

            for (int i = 0; i < titles.length; i++) {
                if (idxOfEmailColumn != -1 && idxOfNameColumn != -1 && idxOfOrganizationColumn != -1)
                    break;

                String title = titles[i].trim();
                if (equalsIgnoreCase("pipeline", title))
                    idxOfEmailColumn = i;
                else if (equalsIgnoreCase("stage", title))
                    idxOfNameColumn = i;
                else if (equalsIgnoreCase("organization", title))
                    idxOfOrganizationColumn = i;
            }

            if (idxOfEmailColumn == -1)
                throw new RuntimeException("Deals file does not have \"Pipeline\" column.");
            if (idxOfNameColumn == -1)
                throw new RuntimeException("Deals file does not have \"Stage\" column.");
            if (idxOfOrganizationColumn == -1)
                throw new RuntimeException("Deals file does not have \"Organization\" column.");


            if (!iterator.hasNext())
                throw new RuntimeException("Deals file has only titles but no data rows.");

            final int idxOfEmailColumnFinal = idxOfEmailColumn;
            final int idxOfNameColumnFinal = idxOfNameColumn;
            final int idxOfOrganizationColumnFinal = idxOfOrganizationColumn;

            return StreamSupport.stream(Spliterators.spliteratorUnknownSize(iterator, 0), true)
                                .filter(row -> {
                                    String pipeline = row[idxOfEmailColumnFinal].trim().toLowerCase();
                                    String stage = row[idxOfNameColumnFinal].trim().toLowerCase();
                                    String organization = row[idxOfOrganizationColumnFinal].trim().toLowerCase();
                                    return pipelinesFilter.contains(pipeline)
                                            && stagesFilter.contains(stage)
                                            && isNotBlank(organization);
                                }).map(row -> row[idxOfOrganizationColumnFinal].trim().toLowerCase())
                                .collect(Collectors.toSet());
        }
    }

}
